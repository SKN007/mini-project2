use aws_sdk_s3::Client as S3Client;
use aws_config::meta::region::RegionProviderChain;
use lambda_http::{run, service_fn, Body, Error, Request, Response};
use serde::{Deserialize, Serialize};
use serde_json::json;
use std::io::Cursor;
use csv::ReaderBuilder;
use std::cmp::Ordering;

#[derive(Debug, Deserialize)]
struct Record {
    value: f64, // Assuming 'value' column contains float data
}

#[derive(Serialize, Deserialize, Default)]
struct ResData {
    max_value: f64,
    min_value: f64,
    values_asc: Vec<f64>,
    values_desc: Vec<f64>,
}

async fn function_handler(event: Request) -> Result<Response<Body>, Error> {
    let region_provider = RegionProviderChain::default_provider().or_else("us-east-1");
    let config = aws_config::from_env().region(region_provider).load().await; // Note: Adjust based on deprecation warning if needed
    let client = S3Client::new(&config);

    let bucket = "skn-mini-project2";
    let key = "mini-project2.csv";
    let resp = client.get_object().bucket(bucket).key(key).send().await?;
    let data = resp.body.collect().await?.into_bytes();
    let cursor = Cursor::new(data);

    let mut rdr = ReaderBuilder::new().from_reader(cursor);
    let mut values: Vec<f64> = Vec::new();

    for result in rdr.deserialize() {
        let record: Result<Record, csv::Error> = result;
        match record {
            Ok(rec) => values.push(rec.value),
            Err(e) => eprintln!("Error reading CSV: {}", e),
        }
    }

    if values.is_empty() {
        return Err(Error::from("No valid data found"));
    }

    values.sort_unstable_by(|a, b| a.partial_cmp(b).unwrap_or(Ordering::Equal));
    let min_value = *values.first().unwrap_or(&0.0);
    let max_value = *values.last().unwrap_or(&0.0);

    let res_data = ResData {
        max_value,
        min_value,
        values_asc: values.clone(),
        values_desc: values.into_iter().rev().collect(),
    };

    let response = Response::builder()
        .status(200)
        .header("Content-Type", "application/json")
        .body(serde_json::to_string(&res_data)?.into())
        .expect("Failed to render response");

    Ok(response)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    run(service_fn(function_handler)).await?;
    Ok(())
}
