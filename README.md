# mini-project 2

A simple AWS Lambda function that processes data.

[Click here see the result](https://6ec4ri41zk.execute-api.us-east-2.amazonaws.com/default/new-lambda-project)
## Requirements

1. Lambda functionality
2. API Gateway integration
3. Data processing
4. Documentation

## Steps

### Data Preparation
1. Create a csv file
2. Create a S3 bucket and upload the csv file (mini-project2.csv in the repo)

### Lambda function
1. Create a new project with cargo
```
cargo lambda new projectName
```
2. Write rust code to obtain csv data from S3 bucket and perform data processing (calculate maximum value, minimum value and sorting)

3. Build the project
```
cargo lambda build
```
4. Test the lambda function
```
cargo lambda watch
```
Then, we can see the result in 127.0.0.1:9000

### Deploy to AWS
1. Create a role that can handle lambda functions and access S3 bucket permissions

2. Deploy with cargo
```
cargo lambda deploy
```

### API Gateway intergation
1. Add a trigger in AWS console, choose API Gateway.

2. Get the API endpoint: https://6ec4ri41zk.execute-api.us-east-2.amazonaws.com/default/new-lambda-project

### Result 
```
{"max_value":23255.0,"min_value":23.34,"values_asc":[23.34,1232.34,2123.44,2332.23,23255.0],"values_desc":[23255.0,2332.23,2123.44,1232.34,23.34]}
```

![](pro2_screenshot.png)

